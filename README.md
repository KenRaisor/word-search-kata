# Word Search Kata

In this an exercise to build a program that completes word search problems.

## Getting Started

Make sure you have [Node.js](http://nodejs.org/) installed.

Install the code as follows:

```sh
$ git clone https://bitbucket.org/KenRaisor/word-search-kata.git
$ cd word-search-kata
$ npm install
```

To run the application
```sh
$ node WordSearch.js -f tests/artifacts/test.txt
```

## Tests

Run the tests by doing the following:

```sh
$ npm test
```


## Authors

* **Ken Raisor** - [BitBucket](https://bitbucket.org/KenRaisor/)




