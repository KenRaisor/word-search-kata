// Module WordSearch
var fs = require('fs');
var SolvePuzzle = {
    solve: function(searchWords,puzzle){
        var resultsObj = {};

        // Convert the text into an array of arrays
        var puzzleMatrix = convertPuzzleIntoMatrix(puzzle);

        // Validate the puzzle's squareness
        if(!checkPuzzleMatrixIsSquare(puzzleMatrix)){
            return "Bad Puzzle: Not Square";
        }

        // loop through each word and search for it in the puzzle matrix
        searchWords.forEach(word => {
            resultsObj[word] = searchPuzzleMatrix(word,puzzleMatrix);
        });

        // validate that all the words were found
        if(!checkResultsHasAllWords(resultsObj)){
            return "Bad Puzzle: Missing Word(s)";
        }

        // format the results and return them
        return formatResults(resultsObj);
    }
};

// Using the compass directions for directions which we will search
var allDirections = [
    "North",
    "NorthEast",
    "East",
    "SouthEast",
    "South",
    "SouthWest",
    "West",
    "NorthWest"
];

// Convert the puzzle text into an array of arrays
var convertPuzzleIntoMatrix = function(puzzleRaw){
    var puzzleMatrix  = [];
    puzzleRaw.split(/\r?\n/).forEach(element => {
        puzzleMatrix.push(element.trim().split(','));
    });
    return puzzleMatrix;
}

// search for a word in the puzzle matrix
var searchPuzzleMatrix = function(word,puzzleMatrix){
    var firstLetter = word.charAt(0);
    // loop through puzzle lines (Y axis)
    for (yindex = 0; yindex < puzzleMatrix.length; ++yindex) {
        
        //loop through the puzzle columns in the line (X axis)
        for (xindex = 0; xindex < puzzleMatrix[yindex].length; ++xindex) {
            
            // find first letter of the word in the puzzle line & get index
            if(puzzleMatrix[yindex][xindex] == firstLetter){    

                // search from this position          
                var allSearchResults =  searchAllDirections(word,xindex,yindex,puzzleMatrix,allDirections);
                if(allSearchResults){
                    
                    // Success!
                    return allSearchResults;
                }
            }
        }
    }
}

// check that the results has all the required words
var checkResultsHasAllWords = function(results){
    for (var Word in results) {
        if(!results[Word]){
            return false;
        }
    }
    return true;
}

// format the resulting data into the required format
var formatResults = function(results){
    // the variable that will contain the final formatted text
    var formatted = '';
    for (var Word in results) {
        formatted += Word + ': ';
        results[Word].forEach(
            coordinate => {
                formatted += '(' + coordinate[0] + ',' + coordinate[1] + '),';
            }
        )
        // remove final comma
        formatted = formatted.substring(0, formatted.length - 1);
        // add final character return
        formatted += '\n';
    }
    return formatted;
}

// a function to contain the the loop for searching in each direction
var searchAllDirections = function(word,xindex,yindex,puzzleMatrix,allDirections){
    for (index = 0; index < allDirections.length; ++index) {
        var directionSearchResults;
        directionSearchResults = searchDirection(word,xindex,yindex,puzzleMatrix, allDirections[index]);
        if(directionSearchResults){
            // success
            return directionSearchResults;
        }
    }
}

// a function to seach in the desired direction for the remaining letters in a word
var searchDirection = function(word,xindex,yindex,puzzleMatrix,direction){
    successCoords = [];
    for (wordindex = 0; wordindex < word.length; ++wordindex) {
        if(!validateCoordinates(xindex,yindex,puzzleMatrix)){
            return null;
        }

        if(puzzleMatrix[yindex][xindex] !== word.charAt(wordindex)){
            return null;
        }
        else{
            successCoords.push([xindex,yindex])
            if(word.length == (wordindex + 1)){
                return successCoords;
            }
            xindex = iterateXOnDirection(xindex,direction);
            yindex = iterateYOnDirection(yindex,direction);
        }
    }
    return successCoords;
}

// a function to determine how the x Index should be iterated based on the direction searched
var iterateXOnDirection= function(xIndex,direction){
    switch(direction) {
        case 'North':
          return xIndex;
          break;
        case 'NorthEast':
          return (xIndex + 1);
          break;
        case 'East':
          return (xIndex + 1);
          break;
        case 'SouthEast':
          return (xIndex + 1);
          break;
        case 'South':
          return xIndex;
          break;
        case 'SouthWest':
          return (xIndex - 1);
          break;
        case 'West':
          return (xIndex - 1);
          break;
        case 'NorthWest':
          return (xIndex - 1);
          break;
      }
}

// a function to determine how the y Index should be iterated based on the direction searched
var iterateYOnDirection= function(yIndex,direction){
    switch(direction) {
        case 'North':
          return (yIndex - 1);
          break;
        case 'NorthEast':
          return (yIndex - 1);
          break;
        case 'East':
          return yIndex;
          break;
        case 'SouthEast':
          return (yIndex + 1);
          break;
        case 'South':
          return (yIndex + 1);
          break;
        case 'SouthWest':
          return (yIndex + 1);
          break;
        case 'West':
          return yIndex;
          break;
        case 'NorthWest':
          return (yIndex - 1);
          break;
      }
}

// Confirm that the coordinates are inside the bounds of the puzzle matrix
var validateCoordinates = function(xindex,yindex,puzzleMatrix){
    if(typeof puzzleMatrix[yindex] === 'undefined'){
        return null;
    }
    if(typeof puzzleMatrix[yindex][xindex] === 'undefined'){
        return null;
    }
    return true;
}

// confirm the square-ness of the puzzle matrix
var checkPuzzleMatrixIsSquare = function(puzzleMatrix){
    for (index = 0; index < puzzleMatrix.length; ++index) {
        if(puzzleMatrix.length !== puzzleMatrix[index].length){
            return false;
        }
    }
    return true;
}

module.exports = SolvePuzzle;
