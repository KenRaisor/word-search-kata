// Module WordSearch
var fs = require('fs');
var ParsePuzzle = {

    getWords: function(fullPuzzle){

        var firstLine = fullPuzzle.split(/\r?\n/)[0];
        return firstLine.split(',');
    },

    getPuzzle: function(fullPuzzle){
        var lines = fullPuzzle.split(/\r?\n/);
        lines.splice(0,1);
        return lines.join('\n');
    },
}
module.exports = ParsePuzzle;
