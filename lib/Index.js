var LoadPuzzle = require(process.cwd() + "/lib/LoadPuzzle.js");
var ParsePuzzle = require(process.cwd() + "/lib/ParsePuzzle.js");
var SolvePuzzle = require(process.cwd() + "/lib/SolvePuzzle.js");

var Index = {
    Start: function(fileName){
        var fileContents = LoadPuzzle.loadFile(fileName);
        var searchWords = ParsePuzzle.getWords(fileContents);
        var puzzle = ParsePuzzle.getPuzzle(fileContents);
        return SolvePuzzle.solve(searchWords,puzzle);
    }
}

module.exports = Index;