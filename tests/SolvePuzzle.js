var chai = require('chai');
var expect = chai.expect; 
var SolvePuzzle = require(process.cwd() + "/lib/SolvePuzzle.js");
var ParsePuzzle = require(process.cwd() + "/lib/ParsePuzzle.js");
describe('word search', function() {
  it('solve() should return "Bad Puzzle" if a puzzle is not solvable', function() {
    var samplePuzzle = 
    "CAT\n"+
    "X,X,X\n"+
    "X,X,X\n"+
    "X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = "Bad Puzzle: Missing Word(s)";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should return "Bad Puzzle" if a puzzle is not square', function() {
    var samplePuzzle = 
    "CAT\n"+
    "C,A,T\n"+
    "X,X,X";

    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = "Bad Puzzle: Not Square";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search horizontally-right and return formatted coordinates', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "B,B,O,N,E,S\n"+
    "A,K,H,A,N,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "BONES: (1,0),(2,0),(3,0),(4,0),(5,0)\n"+
    "KHAN: (1,1),(2,1),(3,1),(4,1)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search Verically and return formatted coordinates', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "B,X,X,X,X,X\n"+
    "O,K,X,X,X,X\n"+
    "N,H,X,X,X,X\n"+
    "E,A,X,X,X,X\n"+
    "S,N,X,X,X,X\n"+
    "X,X,X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "BONES: (0,0),(0,1),(0,2),(0,3),(0,4)\n"+
    "KHAN: (1,1),(1,2),(1,3),(1,4)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search diagnally right-down and return formatted coords', function() {
    var samplePuzzle = 
    "CAT,DOG\n"+
    "C,D,X,X\n"+
    "X,A,O,X\n"+
    "X,X,T,G\n"+
    "X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "CAT: (0,0),(1,1),(2,2)\n"+
    "DOG: (1,0),(2,1),(3,2)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });
  
  it('solve() should search horizontally-left and return formatted coordinates', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "S,E,N,O,B,S\n"+
    "A,N,A,H,K,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X\n"+
    "X,X,X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "BONES: (4,0),(3,0),(2,0),(1,0),(0,0)\n"+
    "KHAN: (4,1),(3,1),(2,1),(1,1)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search Verically-up and return formatted coordinates', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "B,X,X,X,X,X\n"+
    "S,N,X,X,X,X\n"+
    "E,A,X,X,X,X\n"+
    "N,H,X,X,X,X\n"+
    "O,K,X,X,X,X\n"+
    "B,X,X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "BONES: (0,5),(0,4),(0,3),(0,2),(0,1)\n"+
    "KHAN: (1,4),(1,3),(1,2),(1,1)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });
  
  it('solve() should search diagnally right-up and return formatted coordinates', function() {
    var samplePuzzle = 
    "CAT,DOG\n"+
    "X,X,T,G\n"+
    "X,A,O,X\n"+
    "C,D,X,X\n"+
    "X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "CAT: (0,2),(1,1),(2,0)\n"+
    "DOG: (1,2),(2,1),(3,0)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search diagnally left-up and return formatted coordinates', function() {
    var samplePuzzle = 
    "CAT,DOG\n"+
    "T,G,X,X\n"+
    "X,A,O,X\n"+
    "X,X,C,D\n"+
    "X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "CAT: (2,2),(1,1),(0,0)\n"+
    "DOG: (3,2),(2,1),(1,0)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });

  it('solve() should search diagnally left-down and return formatted coordinates', function() {
    var samplePuzzle = 
    "CAT,DOG\n"+
    "X,X,C,D\n"+
    "X,A,O,X\n"+
    "T,G,X,X\n"+
    "X,X,X,X";
    var searchWords = ParsePuzzle.getWords(samplePuzzle);
    var puzzle = ParsePuzzle.getPuzzle(samplePuzzle);
    var solution = 
    "CAT: (2,0),(1,1),(0,2)\n"+
    "DOG: (3,0),(2,1),(1,2)\n";

    expect(SolvePuzzle.solve(searchWords,puzzle)).to.equal(solution); // deep equal
  });
});
