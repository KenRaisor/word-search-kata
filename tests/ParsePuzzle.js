var chai = require('chai');
var expect = chai.expect; 
var ParsePuzzle = require(process.cwd() + "/lib/ParsePuzzle.js");
describe('word search', function() {

  it('getWords() should return an array of the first lines in a string', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "B,O,N,E,S,K,H,A,N";
    expect(ParsePuzzle.getWords(samplePuzzle)).to.eql(['BONES','KHAN']); // deep equal
  });

  it('getPuzzle() should return an string of the puzzle found in text file', function() {
    var samplePuzzle = 
    "BONES,KHAN\n"+
    "B,O,N,E,S,K,H,A,N";
    expect(ParsePuzzle.getPuzzle(samplePuzzle)).to.equal("B,O,N,E,S,K,H,A,N"); // deep equal
  });

});
