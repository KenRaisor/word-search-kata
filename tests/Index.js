var chai = require('chai');
var expect = chai.expect; 
var index = require(process.cwd() + "/lib/index.js");
describe('Index', function() {

  it('index.start() should accept a filename of a valid puzzle file and should correct soled puzzle results',function(){
    var solution = 
    "BONES: (0,6),(0,7),(0,8),(0,9),(0,10)\n"+
    "KHAN: (5,9),(5,8),(5,7),(5,6)\n"+
    "KIRK: (4,7),(3,7),(2,7),(1,7)\n"+
    "SCOTTY: (0,5),(1,5),(2,5),(3,5),(4,5),(5,5)\n"+
    "SPOCK: (2,1),(3,2),(4,3),(5,4),(6,5)\n"+
    "SULU: (3,3),(2,2),(1,1),(0,0)\n"+
    "UHURA: (4,0),(3,1),(2,2),(1,3),(0,4)\n";
    expect(index.Start('./tests/artifacts/test.txt')).to.equal(solution);
  });


});
