var Index = require(process.cwd() + "/lib/Index.js");

var filenameFlag = process.argv[2];
var filename = process.argv[3];

if(filenameFlag == '-f'){
    console.log("Loading file:" + filename);
    var solution = Index.Start(filename);
    console.log("Puzzle Solution:\n" + solution);    
}
